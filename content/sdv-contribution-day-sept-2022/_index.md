---
title: "SDV Contribution Day - September 2022"
headline: ''
custom_jumbotron: |
    <h1 class="event-title text-center">SDV Contribution Day - September 2022</h1>
    <p class="event-subtitle text-center">See how “code first” becomes operational</p>
    <div class="more-detail jumbotron-register-container">
        <a class="btn btn-primary" href="https://www.eventbrite.com/e/sdv-contribution-day-september-2022-tickets-391160139507">Register Now</a>
        <div class="more-detail-text">
            <p>Hybrid Event | September 22, 2022</p>
            <p class="data-location">"Plaza" Space, Deutsche Telekom AG, Friedrich-Ebert-Allee 140, 53113 Bonn</p>
        </div>
    </div>

date: 2022-07-19T15:21:15-04:00
hide_page_title: true
hide_headline: true
hide_sidebar: true
header_wrapper_class: 'header-sdv-contribution-day-2022 header-overlay'
hide_breadcrumb: true
container: 'container-fluid sdv-contribution-day-2022-container'
summary: ''
layout: single
---
<!-- About the Event -->
{{< grid/section-container id="registration" containerClass="padding-bottom-40 padding-top-40" >}}
    {{< grid/div class="container text-center" isMarkdown="false" >}}
        {{< events/registration event="sdv-contribution-day-sept-2022" title="About the Event" >}}
Building on the success of the first SDV Contribution Day, new contributions for open source projects related to the Eclipse Software Defined Vehicle (SDV) Working Group will be presented.

The event is open to any developer, product manager, architect, or business manager interested in topics related to the software defined vehicle.

The SDV Working Group mission is to foster collaboration across industries to create an open technology platform for the software defined vehicle of the future. The working group community has chosen a “code first” approach to facilitate more agile and faster time-to-market software development.

[Information for onsite attendees](./onsite-info)
        {{</ events/registration >}}
    {{</ grid/div >}}
{{</ grid/section-container >}}

<!-- Speakers -->
{{< grid/section-container id="speakers" class="grey-row">}}
  {{< events/user_display event="sdv-contribution-day-sept-2022" source="speakers" imageRoot="images/" title="Speakers">}}
{{</ grid/section-container >}}
{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}


<!-- Agenda -->
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda">}}
  {{< events/agenda event="sdv-contribution-day-sept-2022" icon_class="fa-download"  >}}
{{</ grid/section-container >}}
{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}


<!-- Participating Member Companies -->
{{< grid/section-container containerClass="" class="row text-center padding-bottom-60 padding-top-40" isMarkdown="false" >}}
    <h2>Thanks to Our Participating Member Companies</h2>
    <ul class="eclipsefdn-members-list list-inline margin-30 flex-center gap-40" data-ml-wg="sdv" data-ml-template="only-logos"></ul>
{{</ grid/section-container >}}

<!-- Videos -->
{{< grid/section-container class="row text-center padding-bottom-60 padding-top-40" isMarkdown="false" >}}
  <h2>Videos from our last SDV Contribution Day</h2>
  {{< youtube "playlist?list=PLy7t4z5SYNaRYI7lH26szREOi9pfqxOUU" >}}
{{</ grid/section-container >}}


<!-- Hosted By -->
{{< grid/div class="row grey-row text-center padding-bottom-60 padding-top-40" isMarkdown="true" >}}
## Hosted by 
SDV Contribution Day is organized by the Eclipse Software Defined Vehicle Working Group and hosted by T-Systems
{{</ grid/div >}}