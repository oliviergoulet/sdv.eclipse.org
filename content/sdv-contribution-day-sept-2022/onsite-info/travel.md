---
title: "Travel"
date: 2022-08-10T12:51:51-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---
**Airport:** Cologne Bonn (CGN)

**Train stations:** Bonn or Siegburg

**Driving:** The Telekom parking garage is located directly behind the Headquarters building, near the corner of Nahum-Goldmann-Allee and Olof-Palme Allee
