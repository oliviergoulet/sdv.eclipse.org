---
title: About
seo_title: About | Software Defined Vehicle | Eclipse Foundation
headline: Join the Software Defined Vehicle Working Group
hide_page_title: true
hide_sidebar: true
page_css_file: public/css/about.css
header_wrapper_class: 'header-alt'
custom_jumbotron: |
      <div>
        <h1>Join the Software Defined Vehicle Working Group</h1>
      </div>       
---

{{< grid/div class="container margin-bottom-10" >}}

# About {  .margin-top-50 .text-center }

{{</ grid/div >}}

{{< grid/div class="container margin-bottom-10" >}}

## What is the Software Defined Vehicle? { .h4 .margin-bottom-30 .margin-top-20 .text-center }

The term software-defined vehicle refers to a transformation where the physical and digital components of an automobile are decoupled and features, functionality, and operations are defined through software. In a fully programmable car, digital components—such as modules for safety, comfort and infotainment, and vehicle performance—would be regularly developed and deployed through over-the-air updates.

{{</ grid/div >}}

{{< grid/div class="container margin-bottom-10" >}}

## What is the SDV Working Group? { .h4 .margin-bottom-30 .margin-top-20 .text-center }

The Eclipse Software Defined Vehicle (SDV) is a Working Group within the <a href="https://www.eclipse.org/">Eclipse Foundation</a> that facilitates open source development of automotive software. The aim is to provide a forum for individuals and organizations to build and promote open source solutions for worldwide automotive industry markets. Using a “code first” approach, SDV-related projects focus on building the industry’s first open source software stacks and associated tooling for the core functionality of a new class of automobile.

The SDV Working Group is designed as a vendor-neutral, member-driven organization that allows users and developers to define the roadmap collaboratively. The Working Group provides a governance framework for open source IP management and collaboration. This includes a Steering Committee that defines and manages the strategy and technical roadmap of the working group. A Technical Advisory Committee recommends which Eclipse Foundation open source projects should be included within the purview of the working group.


{{</ grid/div >}}

{{< grid/div class="container margin-bottom-40" >}}

## Why Open Source? { .h4 .margin-bottom-30 .margin-top-20 .text-center }

Current automotive industry paradigms pose challenges to achieving the SDV vision. Today’s car models contain custom hardware and software components sourced from many suppliers. Hardware and software components are tightly integrated and released simultaneously. This results in fragmentation and monolithic programming frameworks. The challenge is illustrated in the following consideration: of the some 100 million lines of code that make up the modern car, it’s estimated that mainstream OS installations of Linux, Windows and OS X share a lot more code between each other than vehicles from any two OEMs.

Open source is a way to confront this complexity and heterogeneity. Collaborating on the technological framework—such as common APIs and hardware abstraction—means OEMs and other industry players do not need to reinvent the wheel, allowing more resources to be devoted to differentiating features and technological advancements. An open source ecosystem also has the potential to accelerate innovation by enabling developers worldwide to solve problems and create applications in the automotive domain.

{{</ grid/div >}}


{{< pages/about/why-should-you-join >}}

{{< pages/about/testimonials >}}

{{< grid/div class="container" id="contact-us" isMarkdown="false">}}
<div class="row row-no-gutters">
  <div class="col-sm-20 col-xs-24 col-sm-offset-2">
    <div class="card-contact">
      <h2>Contact us about Membership</h2>
      {{< hubspot_contact_form portalId="5413615" formId="8bf9c8c1-32c8-4e37-8956-b7420e3388cb" >}}
    </div>
  </div>
</div>
{{</ grid/div >}}
