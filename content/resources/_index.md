---
title: "Resources"
seo_title: "Resources - Software Defined Vehicle"
date: 2022-08-29T10:39:34-04:00
description: ""
categories: []
keywords: ["Software Defined Vehicle", "SDV"]
slug: ""
aliases: []
container: 'container'
layout: single
hide_sidebar: false

---

Below are some useful links to articles and other resources about Eclipse SDV.

## Videos

{{< youtube "playlist?list=PLy7t4z5SYNaRYI7lH26szREOi9pfqxOUU">}}

{{< grid/div class="margin-top-20 text-right" isMarkdown="false" >}}
  <a class="resources-videos-view-more" href="./videos">View More</a>
{{</ grid/div >}}
