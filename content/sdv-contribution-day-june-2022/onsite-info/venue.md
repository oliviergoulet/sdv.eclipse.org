---
title: "Venue"
date: 2022-08-10T13:08:49-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---
The venue for SDV Contribution Day is the ZF Forum.

[ZF Forum](https://www.zf.com/site/zfforum/en/homepage/zf_forum.html)

Löwentaler Straße 20 

88046 Friedrichshafen

Germany