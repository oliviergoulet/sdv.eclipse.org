---
title: Eclipse SDV Hackathon Challenge
seo_title: Eclipse SDV Hackathon Challenge | Software Defined Vehicle
headline: ''
custom_jumbotron: |
  <div class="subnav">
    <nav class="container">
      <ul class="list-unstyled">
        <li>
          <a href="#about">Hackathon</a>
        </li>
        <li>
          <a href="#rules">Rules</a>
        </li>
        <li>
          <a href="#venue-and-location">Location</a></li>
        <li>
          <a href="#schedule">Schedule</a>
        </li>
        <li>
          <a class="btn btn-neutral" href="https://www.eventbrite.com/e/sdv-hackathon-accenture-tickets-679248198647?aff=oddtdtcreator">
            Register
          </a>
        </li>
      </ul>
    </nav>
  </div>
  <div class="custom-jumbotron-content">
    <h1>Eclipse SDV <br><span class="highlight">Hackathon</span> Challenge</h1>
    <div class="custom-jumbotron-content-bg">
      <img class="img img-responsive" src="./images/featured-jumbotron-middle-bg.jpg" alt="" />
    </div>
    <div class="custom-jumbotron-content-body">
      <p class="jumbotron-subtitle">
        Build the car of the future - use current Eclipse SDV projects to solve
        real automotive challenges!
      </p>
      <p class="jumbotron-text">
        You don't have to be a rocket scientist to help build the next
        Rocketmobile! We start with code first! Join the Eclipse SDV Hackathon
        Challenge held at Accenture Munich office to hack the software defined
        car of the future!
      </p>
      <p class="jumbotron-tagline">
        <small>
          November 28 - 30, 2023
          <br>
          Accenture | Munich, Germany
        </small>
      </p>
      <div class="custom-jumbotron-gradient"></div>
    </div>
  </div>
header_wrapper_class: sdv-hackathon-2023-landing-page
hide_headline: true
hide_breadcrumb: true
layout: single
container: container-fluid landing-page
cascade:
  page_css_file: /public/css/sdv-hackathon-2023.css
  hide_page_title: true
  hide_sidebar: true
---

{{< pages/sdv-hackathon-2023/about >}}

{{< pages/sdv-hackathon-2023/hack_challenges >}}

{{< pages/sdv-hackathon-2023/what_to_expect >}}

{{< grid/section-container class="section-lip padding-y-60" isMarkdown="true" >}}

## Who Can Join?

This hackathon is open to all. Whether you are a student just starting with
coding or you are an experienced software developer and interested in hacking
the challenges of automotive software, you are welcome to join the quest!

Make sure to attend and get hands-on experience on our open source tools and
projects.

If you are still not acquainted with open source and SDV, it will be useful to
check out our current [SDV Projects](/projects/) and get to know what you can
expect.

{{</ grid/section-container >}}

{{< pages/sdv-hackathon-2023/additional_info >}}

{{< grid/section-container id="hack-mcs" class="hack-mcs-section section-featured section-lip padding-top-60">}}
  {{< events/user_display event="sdv-hackathon-2023" source="hack-mcs" imageRoot="images/mentors/" title="HACK MCs">}}
  {{</ events/user_display >}}
{{</ grid/section-container >}}

{{< grid/section-container id="hack-coaches" class="section-featured">}}
  {{< events/user_display event="sdv-hackathon-2023" source="hack-coaches" imageRoot="images/mentors/" title="Hack Coaches">}}
  {{</ events/user_display >}}
{{</ grid/section-container >}}

{{< grid/section-container id="jury" class="section-featured padding-bottom-60">}}
  {{< events/user_display event="sdv-hackathon-2023" source="jury" imageRoot="images/jury/" subpage="jury" displayLearnMore="false" title="Jury" >}}
  {{</ events/user_display >}}
  <a class="btn btn-primary" href="./jury">Learn more about the jury</a>
{{</ grid/section-container >}}

{{< pages/sdv-hackathon-2023/rules >}}

{{< pages/sdv-hackathon-2023/venue_and_location >}}

{{< grid/section-container class="section-lip padding-top-60" isMarkdown="false" >}}
  <h2>Partners & Sponsors</h2>
  <div class="col-sm-18 col-sm-offset-3 margin-top-40 margin-bottom-40">
    <ul class="flex-center list-inline gap-60">
      <li>
        <a href="/">
          <img src="./images/sponsors/software-defined-vehicle.svg" alt="Eclipse Software Defined Vehicle" width="200" />
        </a>
      </li>
      <li>
        <a href="https://eclipse.org/">
          <img src="./images/sponsors/eclipse-foundation.svg" alt="Eclipse Foundation" width="200" />
        </a>
      </li>
    </ul>
  </div>
  <div class="col-sm-24 margin-y-20">
    <p class="font-size-lg">Contact us at <a href="mailto:events@eclipse-foundation.org">events@eclipse-foundation.org</a>.
  </div>
{{</ grid/section-container >}}

{{< grid/section-container class="padding-bottom-40" isMarkdown="false" >}}
  <h2>Facility and Hospitality</h2>
  <div class="col-sm-18 col-sm-offset-3 margin-top-40 margin-bottom-40">
    <ul class="flex-center list-inline gap-60">
      <li>
        <a href="https://www.accenture.com/">
          <img src="./images/facility/accenture-logo.png" alt="Accenture" class="accenture-logo" width="200"/>
        </a>
      </li>
    </ul>
  </div>
{{</ grid/section-container >}}

{{< grid/section-container class="bg-deep-violet dark padding-y-60" isMarkdown="false" >}}
  <h2 id="code-of-conduct">Code of Conduct</h2>
  <p>
    Please make sure you read our Code of Conduct before joining the event and
    follow it through during the entire hackathon.
  </p>
  <a class="btn btn-primary margin-top-40" href="https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php">Read Code of Conduct</a>
{{</ grid/section-container >}}
