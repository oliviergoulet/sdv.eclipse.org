---
title: "Projects"
seo_title: "Projects - Software Defined Vehicle"
headline: The SDV Projects Landscape
hide_sidebar: true
hide_page_title: true
header_wrapper_class: 'header-alt'
custom_jumbotron: |
      <div>
        <h1>The SDV Projects Landscape</h1>
      </div> 
---

{{< project_landscape >}}

{{< grid/div class="row text-center padding-bottom-40" isMarkdown="false" >}}
    {{< grid/div class="container" isMarkdown="false" >}}
        {{< grid/div class="row" isMarkdown="false" >}}
            {{< grid/div class="col-sm-18 col-sm-offset-3" isMarkdown="false" >}}
                <h1 class="margin-bottom-40">Projects</h1>
                <p>The Software Defined Vehicle Working Group serves to
                advance, promote, and drive community participation in a
                broad portfolio of Eclipse projects.</p>
            {{</ grid/div >}}
        {{</ grid/div >}}
    {{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="container" isMarkdown="false" >}}
    {{< grid/div class="row" isMarkdown="false" >}}
        {{< eclipsefdn_projects
            templateId="tpl-projects-item"
            url="https://projects.eclipse.org/api/projects?working_group=eclipse-software-defined-vehicle"
            classes="margin-top-30"
            display_categories="false"
            categories=""
            display_view_more="false"
        >}}
    {{</ grid/div >}}
{{</  grid/div >}}
