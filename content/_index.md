---
title: "Software Defined Vehicle"
seo_title: "Software Defined Vehicle"
headline: "Software Defined Vehicle"
tagline: "An open technology platform for the software defined vehicle of the future; accelerating innovation of automotive software stacks through a vibrant open source community."
date: 2021-05-04T10:00:00-04:00
layout: "single"
hide_breadcrumb: true
hide_sidebar: true
hide_page_title: true
show_featured_story: true
links: [[href: "/members/", text: "Get Involved"],[href: "https://accounts.eclipse.org/mailing-list/sdv-wg", text: "Connect"]]
---

{{< project_landscape includeTitle="true" >}}

{{< pages/home/about_section >}}

<!-- Event Calendar section -->
{{< grid/div class="row text-center padding-top-60" isMarkdown="false" >}}
  <h2 class="margin-top-0 margin-bottom-20">Don't Miss an Event</h2>
  <a class="btn btn-primary" href="https://calendar.google.com/calendar/u/0?cid=Y18yYW1waTJibW9rYTNxdGVyNGRjZWFwMWQ1Z0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t">Add to Your Calendar</a>
{{</ grid/div >}}

{{< pages/home/members_section >}}

{{< pages/home/events_section >}}

{{< pages/home/whats_new_section >}}
  {{< newsroom/news
    id="news-list-container" 
    count="3"
    paginate="true"
    class="news-list" 
    publishTarget="sdv" 
    templateId="cards"
  >}}
{{</ pages/home/whats_new_section >}}

{{< pages/home/video_section >}}

{{< mustache_js template-id="tpl-event-timeline" path="/js/templates/event-timeline.mustache" >}}
