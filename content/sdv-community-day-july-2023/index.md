---
title: SDV Community Day July 2023
date: 2023-05-01T15:29:46-04:00
summary: >-
    Meet and engage with the thriving SDV Community and work towards building
    an SDV Distribution entirely consisting of Open Source Projects.
description: >-
    Meet and engage with the thriving SDV Community and work towards building
    an SDV Distribution entirely consisting of Open Source Projects.
categories: []
keywords: []
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">SDV Community Day at ZF Forum - July 2023</h1>
    <p class="featured-jumbotron-subtitle text-center">
        Meet and engage with the thriving SDV Community and work towards building
        an SDV Distribution entirely consisting of Open Source Projects.
    </p>
    <div class="more-detail jumbotron-register-container">
        <div class="more-detail-text padding-top-40">
            <p>Hybrid Event | July 6, 2023 | 09:00 CEST</p>
            <p class="data-location">ZF Forum | Friedrichshafen, Germany</p>       
            <ul class="list-inline">
                <li class="margin-top-10">
                    <a class="btn btn-light btn-lg" href="https://www.eventbrite.com/e/sdv-community-day-at-zf-forum-july-2023-tickets-631967671317">
                        Register Now
                    </a>
                    
                </li>
            </ul> 
        </div>
    </div>
hide_page_title: true
hide_headline: true
hide_sidebar: true
hide_breadcrumb: true
header_wrapper_class: header-community-day-2023
container: container-fluid sdv-contribution-day-2022-container
page_css_file: /public/css/sdv-community-day-july-2023.css
resources:
    - src: "*.md"
---

<!-- About the Event -->
{{< grid/section-container id="registration" containerClass="padding-bottom-40 padding-top-40" >}}
    {{< grid/div class="container text-center" isMarkdown="false" >}}
        {{< events/registration event="sdv-community-day-july-2023" title="About the Event" >}}

With the number of successful Eclipse SDV projects growing rapidly, we’re happy
to announce the next SDV Community Day at the ZF Forum in Friedrichshafen.

The SDV Community Day is meant has become a platform where developers can meet,
interact with each other and present to their peers the SDV projects they have
been working on.

The goal of the Eclipse SDV Working Group now is to build an Eclipse SDV
Distribution  entirely made of Open Source Projects. Decision makers are
welcome to join and interact with the community they help create.

The event is open to any developer, product manager, architect, or business
manager interested in topics related to the software defined vehicle.

The SDV Working Group mission is to foster collaboration across industries to
create an open technology platform for the software defined vehicle of the
future. The working group community has chosen a “code first” approach to
facilitate more agile and faster time-to-market software development.

        {{</ events/registration >}}
        {{< previous_event_videos >}}
    {{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container containerClass="container text-center padding-top-40 padding-bottom-60" isMarkdown="false" >}}
  <h2>Community Days Timeline</h2>
  <img 
    class="img img-responsive margin-auto margin-top-40" 
    src="images/timeline.png" 
    alt="May 8: Event registration opens. May 11: Info meeting for new organizations. June 9: Call for proposals deadline for tech talks and presentations. June 23: Project proposal deadline and public review started. July 6: Q2 Community Day Eclipse Foundation event at ZF Friedrichshafen." 
  />
{{</ grid/div >}}

<!--- Agenda -->
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda" >}}
    {{< pages/sdv-community-day-2023/agenda event="sdv-community-day-july-2023" >}}
    <small>* All times displayed are in Central European Summer Time (CEST).</small>
{{</ grid/section-container >}}
{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}

<!-- Onsite -->
{{< grid/section-container containerClass="container" class="row text-center padding-bottom-60 padding-top-40" isMarkdown="false" >}}
    <h2>Information for Onsite Attendees</h2>
    <img class="lisbon-2023-onsite-image" src="./images/zf-hq.jpg" alt="ZF Forum" />
    {{< info_list resource="onsite-info/*.md" >}}
{{</ grid/div >}}

<!-- Hosted By -->
{{< grid/div class="row grey-row text-center padding-bottom-60 padding-top-40" isMarkdown="true" >}}

## Hosted by ZF 

SDV Community Day is organized by the Eclipse Software Defined Vehicle Working Group and hosted by ZF. 

{{</ grid/div >}}

<!-- Participating Member Companies -->
{{< grid/section-container containerClass="" class="row grey-row text-center padding-bottom-60 padding-top-40" isMarkdown="false" >}}
    <h2>Thanks to Our Participating Member Companies</h2>
    <ul class="eclipsefdn-members-list list-inline margin-30 flex-center gap-40" data-ml-wg="sdv" data-ml-template="only-logos"></ul>
{{</ grid/section-container >}}

