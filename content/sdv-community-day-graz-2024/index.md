---
title: SDV Community Day - Graz March 2024
date: 2024-01-30T15:29:46-04:00
summary: >-
    Meet and engage with the thriving SDV Community and work towards building
    an SDV Distribution entirely consisting of Open Source Projects.
description: >-
    Meet and engage with the thriving SDV Community and work towards building
    an SDV Distribution entirely consisting of Open Source Projects.
keywords: ["event", "automotive", "graz", "contribution", "community"]
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">SDV Community Day Graz</h1>
    <div class="more-detail jumbotron-register-container text-center">
        <div class="more-detail-text">
            <p>Hybrid Event | Mar 19-20, 2024</p>
            <p class="data-location">
                <span class="uppercase">Helmut List Halle</span> | Graz, Austria
            </p>       
            <ul class="list-inline">
                <li class="margin-top-10">
                    <a class="btn btn-light btn-lg" href="https://eclipsecon.regfox.com/sdv-community-day-graz-2024">
                        Register Now
                    </a>
                </li>
            </ul> 
        </div>
    </div>
hide_page_title: true
hide_headline: true
hide_sidebar: true
hide_breadcrumb: true
header_wrapper_class: header-community-day-2023
container: event container-fluid 
page_css_file: /public/css/sdv-community-day-eclipsecon-2023.css
resources:
  - src: "*.md"
---

{{< grid/section-container id="registration" containerClass="padding-bottom-40 padding-top-40" >}}
    {{< grid/div class="container text-center" isMarkdown="false" >}}
        {{< events/registration event="sdv-community-day-graz-2024" title="About the Event" >}}

Building on the success of the SDV Contribution Days, we’ve established the
need to meet up, present new contributions and projects, and learn from field
experts from and outside of the community. 

With a number of successful projects, we are setting ourselves the goal of
building an Eclipse SDV Distribution entirely made of Open Source Projects.
This event is a platform where developers can be the center of the attention
and interact with one another, and present to the peers what they have been
working on.

The SDV Community Days event is targeted towards developers of existing
projects, and interested developers, as well as organizations, that are not yet
actively involved in SDV related projects. Decision makers are welcome to join
and interact with the community they help create.

        {{</ events/registration >}}
    {{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container class="featured-section-row padding-y-60" isMarkdown="false" >}}
  <h2>Our Previous Events</h2>
  {{< carousel id="carousel-images" >}}
    {{< carousel_item image="./images/carousel/slide-1.jpg" alt="A group of three attendees standing at a table, having a discussion." >}}
    {{< carousel_item image="./images/carousel/slide-2.jpg" alt="Three attendees wearing eCalize It shirts speaking with another attendee." >}}
    {{< carousel_item image="./images/carousel/slide-3.jpg" alt="An auditorium with attendees watching the presentation given by an SDV speaker." >}}
    {{< carousel_item image="./images/carousel/slide-4.jpg" alt="All attendees posing for a group photo in front of the Microsoft office in Lisbon." >}}
    {{< carousel_item image="./images/carousel/slide-5.jpg" alt="A speaker holding a microphone giving a talk." >}}
    {{< carousel_item image="./images/carousel/slide-6.jpg" alt="A speaker with a uProtocol t-shirt giving a presentation to a group of attendees." >}}
    {{< carousel_item image="./images/carousel/slide-7.jpg" alt="A speaker giving a talk. The current slide in his presentation is titled 'Component View of Eclipse Developer Console'" >}}
    {{< carousel_item image="./images/carousel/slide-8.jpg" alt="Person talking to a group of people in a public space meant for talks within the Microsoft office building. Three cameras are filming the speaker." >}}
    {{< carousel_item image="./images/carousel/slide-9.jpg" alt="Speaker giving a talk to a group of people in a public space. A large screen hangs overhead with the speaker's slides." >}}
    {{< carousel_item image="./images/carousel/slide-10.jpg" alt="Speaker giving a talk to a group of people in a public space. They are displaying code in their slide." >}}
  {{</ carousel >}}
  {{< previous_event_videos class="margin-y-40" hide_heading="true" >}}
{{</ grid/section-container >}}

{{< grid/section-container class="featured-section-row padding-y-60" isMarkdown="false" >}}

<h2 id="participating-members">
    Thanks to Our Participating Member Companies
</h2>

<ul 
  class="eclipsefdn-sdv-members-list list-inline gap-40 margin-top-40 flex-center" 
  data-ml-template="carousel" 
  data-ml-wg="sdv"
  data-ml-count="4"
>
</ul>

{{</ grid/section-container >}}

<!-- Agenda -->
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda" isMarkdown="false" >}}
  {{< events/agenda event="sdv-community-day-graz-2024" icon_class="fa-download" >}}
  <small>* All times displayed are in Central European Time (CET).</small>
{{</ grid/section-container >}}

<!-- Onsite -->
{{< grid/section-container containerClass="container" class="featured-section-row grey-row padding-y-60" isMarkdown="false" >}}
    <h2 id="onsite-info">Information for Onsite Attendees</h2>
    {{< info_list resource="onsite-info/*.md" >}}
{{</ grid/section-container >}}

{{< grid/section-container class="featured-section-row padding-y-60" isMarkdown="true" >}}

## Hosted by AVL/AT

SDV Community Days is hosted by the Eclipse Software Defined Vehicle Working
Group

{{</ grid/section-container >}}
