/*!
 * Copyright (c) 2021, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *   Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require('./node_modules/eclipsefdn-solstice-assets/webpack-solstice-assets.mix.js');
let mix = require('laravel-mix');
mix.EclipseFdnSolsticeAssets();

mix.setPublicPath('static/public');
mix.setResourceRoot('../');

mix.less('./less/styles.less', 'static/public/css/styles.css');
mix.less('./less/pages/sdv-community-day-eclipsecon-2023.less', 'static/public/css/sdv-community-day-eclipsecon-2023.css');
mix.less('./less/pages/sdv-community-day-july-2023.less', 'static/public/css/sdv-community-day-july-2023.css');
mix.less('./less/pages/sdv-hackathon-2023.less', 'static/public/css/sdv-hackathon-2023.css');
mix.less('./less/pages/about.less', 'static/public/css/about.css');
mix.less('./less/pages/members.less', 'static/public/css/members.css');

mix.js('js/main.js', './static/public/js/solstice.js');
