/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */


// This file serves as a hotfix for the SDV members list.

import renderTemplate from 'eclipsefdn-solstice-assets/js/solstice/templates/render-template';
import templateLoading from 'eclipsefdn-solstice-assets/js/solstice/templates/loading-icon.mustache';
import onlyLogosTemplate from 'eclipsefdn-solstice-assets/js/solstice/templates/wg-members/wg-member-only-logos.mustache';
import carouselTemplate from './templates/wg-members/carousel.mustache';
import { validateURL } from 'eclipsefdn-solstice-assets/js/utils/utils';

const apiBasePath = 'https://membership.eclipse.org/api';

export const organizationMapper = organization => {
  const { organization_id: organizationId, ...otherOrganizationFields } = organization;

  return {
    organizationId,
    ...otherOrganizationFields,
  };
};

/** 
  * Creates a function which filters an array of organizations based on
  * provided level and, if provided, the working group.
  *
  * @param {string[]} level - The levels of the organization to filter by (ex.
  * SD, AP, etc.)
  * @param {string|undefined} workingGroupId - The working group of the
  * organization.
  *
  * @returns {function} - A function that can be used to filter an array of
  * members.
  */
const filterLevel = (level, workingGroupId) => (organization) => {
  let formattedLevel;
  // For certain levels, we need to map them to their corresponding
  // level in the API because they are not simply prefixed with WG.
  const levelMap = { AP: 'WGAPS' };
  
  level = level.map((l) => {
    if (levelMap[l] !== undefined) {
      // If the level is in the map, use the corresponding value.
      formattedLevel = levelMap[l];
    } else {
      // Otherwise, prefix the level with WG if it isn't already. If it is,
      // just use the level as-is.
      formattedLevel = l.startsWith('WG') ? l : `WG${l}`;
    }

    return formattedLevel
  });

  return organization.wgpas.find((wgpa) => {
    // Base condition, match level according to parameter.
    const hasMatchingLevel = level.find((l) => wgpa.level === l) !== undefined;

    // Filter out collaborations if provided.
    if (workingGroupId) {
      return hasMatchingLevel && wgpa.working_group === workingGroupId;
    }

    return hasMatchingLevel;
  });
};

/**
  * Get working group participating organizations
  *
  * @param {string} workingGroupId - The id of the working group.
  * @param {object} options - The options to configure the request.
  * @param {string[]} options.level - The level of the organization (ex. SD,
  * AP, etc.)
  */
export const getWorkingGroupParticipatingOrganizations = async (workingGroupId, options = {}) => {
  try {
    if (!workingGroupId) {
      throw new Error('No working group id provided for fetching participating organizations');
    }

    const url = `${apiBasePath}/organizations?working_group=${workingGroupId}`;
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Could not fetch participating organizations for working group id "${workingGroupId}"`);
    }

    let data = await response.json();

    if (options.level) {
      data = data.filter(filterLevel(options.level, workingGroupId));
    }

    const organizations = data.map(organizationMapper);

    return [organizations, null];
  } catch (error) {
    console.error(error);
    return [null, error];
  }
};

const templates = {
  'only-logos': onlyLogosTemplate,
  'carousel': carouselTemplate,
}

/**
  * Provides a fallback logo for organizations that do not have a logo, or if
  * their logo is invalid.
  */
const urlLinkToLogo = function () {
  if (this.website !== null && validateURL(this.website)) {
    return this.website;
  }
  return `https://www.eclipse.org/membership/showMember.php?member_id=${this.organizationId}`;
};

const render = () => {
  const elements = document.querySelectorAll('.eclipsefdn-sdv-members-list');

  elements.forEach(async (element) => {
    const workingGroupId = element.dataset.mlWg;
    let level = element.dataset.mlLevel
    if (level) {
      // If level is provided, we need to split it into an array.
      level = level.split(',').map((level) => level.trim());
    }
    const templateId = element.dataset.mlTemplate;
    // If the template isn't the built-in "carousel" template, then we need to
    // rely on the "isCarousel" data attribute.
    const isCarousel = element.dataset.isCarousel || templateId === 'carousel';

    element.innerHTML = templateLoading();

    let [members, error] = await getWorkingGroupParticipatingOrganizations(workingGroupId, { level });

    if (members.length === 0 || error) {
      element.innerHTML = `<p class="alert alert-danger">Could not retrieve members. Please try again another time.</p>`;
      return;
    }

    const templateData = {
      item: members,
      urlLinkToLogo,
    }

    await renderTemplate(
      element,
      templates,
      templateId,
      templateData,
    );

    if (isCarousel) {
      reinitializeCarousel(element);
    }
  });
};

export default { render };

const reinitializeCarousel = (element, count = 5) => {
  const carouselElement = element.querySelector('.owl-carousel');
  const owl = $(carouselElement);
  if (owl.data('owl.carousel')) {
    owl.trigger('destroy.owl.carousel');
  }

  owl.owlCarousel({
    dots: false,
    items: 1,
    responsive: {
      568: {
        dots: true,
        items: count,
      },
    }
  });
  
  carouselElement.style.display = 'block';
};
